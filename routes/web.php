<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get("hello",function(){
    return "xin chao";
});
Route::get('database',function(){
    Schema::create('user',function($table){
        $table->increments('id');
        $table->string('name',50);
        $table->string('email',50);
    });
});
Route::get('themcot',function(){
    Schema::table('user',function($table){
        $table->string('password',20);
    });
});
Route::get('test','Usercontroller@hello');
Route::get('MyRequest','Usercontroller@myrequest');
